defmodule Egraphql.ParserTest do
  use ExUnit.Case, async: true
  alias Egraphql.Parser, as: Parser

  # Tokenization tests

  @token_tests  [
      {"345", [{:int, 1, 345}]},
      {"-345", [{:int, 1, -345}]},
      {"false", [{:bool, 1, false}]},
      {"true", [{:bool, 1, true}]},
      {"myname", [{:name, 1, "myname"}]},
      {"\"\"", [{:string, 1, ""}]},
      {"\"a string\"", [{:string, 1, "a string"}]},
      {" \t \r \n  ", []},
      {",", []},
      {"#", []},
      {"# a comment", []},
      {"# a comment\n 456", [{:int, 2, 456}]},
      {"@", [{:'@', 1}]},
      {"@", [{:'@', 1}]},
      {"@", [{:'@', 1}]},
      {":", [{:':', 1}]},
      {"{", [{:'{', 1}]},
      {"}", [{:'}', 1}]},
      {"(", [{:'(', 1}]},
      {")", [{:')', 1}]},
      {"[", [{:'[', 1}]},
      {"]", [{:']', 1}]},
      {"$", [{:'$', 1}]},
      {"=", [{:'=', 1}]},
      {"...", [{:'...', 1}]},
      {"!", [{:'!', 1}]}
  ]

  for {str, expt} <- @token_tests do
    @str str
    @expt expt
    test "tokenizing '#{@str}'" do
      {:ok, tokens, _} = Parser.tokenize(@str)
      assert @expt == tokens
    end
  end
end
