Definitions.

INT        = -*[0-9]+
WS         = [\s\t\n\r]
COMMENT    = #[^\n]*
COMMA      = ,
NAME       = [_A-Za-z][_0-9A-Za-z]*
BOOL       = (true|false)
CHAR       = [^\"\n\r]
STRING     = \"{CHAR}*\"

Rules.

{BOOL}       : {token, {bool, TokenLine, to_bool(TokenChars)}}.
{INT}        : {token, {int, TokenLine, list_to_integer(TokenChars)}}.
{NAME}       : {token, {name, TokenLine, list_to_bitstring(TokenChars)}}.
{STRING}     : {token, {string, TokenLine, strip_string(TokenChars)}}.

\[           : {token, {'[',  TokenLine}}.
\]           : {token, {']',  TokenLine}}.
\(           : {token, {'(',  TokenLine}}.
\)           : {token, {')',  TokenLine}}.
\{           : {token, {'{',  TokenLine}}.
\}           : {token, {'}',  TokenLine}}.
\!           : {token, {'!',  TokenLine}}.
\@           : {token, {'@',  TokenLine}}.
\:           : {token, {':',  TokenLine}}.
\=           : {token, {'=',  TokenLine}}.
\$           : {token, {'$',  TokenLine}}.
\.\.\.       : {token, {'...',  TokenLine}}.

{COMMENT}    : skip_token.
{COMMA}      : skip_token.
{WS}+        : skip_token.

Erlang code.

to_bool("true") -> true;
to_bool(_) -> false.

strip_string([_|T]) ->
  list_to_bitstring(string:sub_string(T, 1, length(T) - 1)).
