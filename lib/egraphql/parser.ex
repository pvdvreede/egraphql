defmodule Egraphql.Parser do
  def tokenize(str) do
    str |> to_char_list |> :graphql_lexer.string
  end
end
